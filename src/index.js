import 'babel-polyfill'
import React from 'react'
import { render } from 'react-dom'
// import App from './containers/App'
//
// import Admin from './components/Admin'
// import Genre from './components/Genre'
// import Home from './components/Home'
// import NotFound from './components/NotFound'
// import List from './components/List'
// import Release from './components/Release'

// import { Router, Route, IndexRoute, browserHistory } from 'react-router'
import { Router, browserHistory } from 'react-router'
import { routes } from './routes'

import { Provider } from 'react-redux'
import configureStore from './store/configureStore'

const store = configureStore()

// render(
// <App />,
//     document.getElementById('root')
// )

// render(
//     <Router history={browserHistory}>
//         <Route path='/' component={App}>
//             <IndexRoute component={Home} />
//             <Route path='admin' component={Admin} />
//             <Route path='genre/:genre' component={Genre} >
//                 {/*<Route path=':title' component={Genre} />
//                  добавили новый route */}
//                 <Route path=':release' component={Release} />
//             </Route>
//             <Route path='list' component={List} />
//             {/* для всех остальных роутов: показывай NotFound */}
//             <Route path='*' component={NotFound} />
//         </Route>
//     </Router>,
//     document.getElementById('root')
// )

// render(
//     <Router history={browserHistory} routes={routes} />,
//     document.getElementById('root')
// )

render(
    <Provider store={store}>
        <Router history={browserHistory} routes={routes} />
    </Provider>,
    document.getElementById('root')
)