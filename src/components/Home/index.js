// import React, { Component } from 'react'
// import { browserHistory } from 'react-router'
import React, { Component, PropTypes } from 'react'

export default class Home extends Component {
    //add constructor, use with context, variant 2
    // https://maxfarseer.gitbooks.io/react-router-course-ru/content/programmiruem_perehodi.html
    constructor() {
        super();
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    componentDidMount() {
        this.context.router.setRouteLeaveHook(this.props.route, this.routerWillLeave)
    }
    routerWillLeave() {
        // let answer = window.confirm('Вы уверены?')
        // if (!answer) return false
        return 'Вы уверены?'
    }
    handleSubmit(e) {
        e.preventDefault()
        const value = e.target.elements[0].value.toLowerCase()
        // browserHistory.push(`/genre/${value}`)
        this.context.router.push(`/genre/${value}`)
    }
    render() {
        return (
            <div className='row'>
                <div className='col-md-12'>Раздел /</div>
                <form className='col-md-4' onSubmit={this.handleSubmit}>
                    <input type='text' placeholder='genreName'/>
                    <button type='submit'>Перейти</button>
                </form>
            </div>
        )
    }
}

Home.contextTypes = {
    router: PropTypes.object.isRequired
}