import React, { Component } from 'react'

// export default class Genre extends Component {
//     render() {
//         return (
//             <div className='row'>
//                 <div className='col-md-12'>Раздел /genre</div>
//             </div>
//         )
//     }
// }

// export default class Genre extends Component {
//     render() {
//         return (
//             <div className='row'>
//                 <h3 className='col-md-12'>{this.props.params.genre}</h3>
//                 <h3 className='col-md-12'>{this.props.params.title}</h3>
//                 <div className='col-md-12'>Здесь будет список релизов</div>
//             </div>
//         )
//     }
// }

export default class Genre extends Component {
    render() {
        let template;
        {/* если параметр release есть - покажи дочерний компонент */}
        if (this.props.params.release) {
            template = (
                <div className='row'>
                    <h3 className='col-md-12'>{this.props.params.genre}</h3>
                    <div className='col-md-12'>{this.props.children}</div>
                </div>
            )
        } else {
            template = (
                <div className='row'>
                    <h3 className='col-md-12'>{this.props.params.genre}</h3>
                    <div className='col-md-12'>Здесь будет список релизов</div>
                </div>
            )
        }

        return template;
    }
}