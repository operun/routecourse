import React from 'react'
import { Route, IndexRoute } from 'react-router'

// import { Route, IndexRedirect } from 'react-router'

import App from './containers/App'
import Admin from './components/Admin'
import List from './components/List'
import Genre from './components/Genre'
import Release from './components/Release'
import Home from './components/Home'
import NotFound from './components/NotFound'
// import Login from './components/Login'
import LoginPage from './containers/LoginPage'
import requireAuthentication from './containers/AuthenticatedComponent'

export const routes = (
    <div>
        <Route path='/' component={App}>
            <IndexRoute component={Home} />
             {/*<IndexRedirect to='list' />*/}
            {/*<Redirect from='/profile/photos' to='/new_all_photos_page'/>*/}
            {/*<Route path='/login' component={Login} />*/}
            <Route path='/login' component={LoginPage} />
            {/*<Route path='/admin' component={Admin} onEnter={checkLogin}/>*/}
            {/*<Route path='/admin' component={Admin} onEnter={Admin.onEnter}/>*/}
            <Route path='/admin' component={requireAuthentication(Admin)} /> {/* Admin -> <Component {...this.props} />*/}
            <Route path='/genre/:genre' component={Genre}>
                <Route path='/genre/:genre/:release' component={Release} />
            </Route>
            <Route path='/list' component={List} />
        </Route>
        <Route path='*' component={NotFound} />
    </div>
)

// function checkLogin(nextState, replace) {
//     const login = window.localStorage.getItem('rr_login')
//     if (login !== 'admin') {
//         replace('/')
//     }
// }