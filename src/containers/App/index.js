import React, { Component } from 'react'
// import Admin from '../components/Admin'
// import Genre from '../components/Genre'
// import Home from '../components/Home'

// import { Link } from 'react-router'

// import './styles.scss'
import NavLink from '../../components/NavLink'

export default class App extends Component {
    render() {
        return (
            <div className='container'>
                <ul className='nav nav-pills'>
                    <li><NavLink onlyActiveOnIndex={true} to='/'>Главная</NavLink></li>
                    <li><NavLink to='/admin'>Админка</NavLink></li>
                    <li><NavLink to='/list'>Список жанров</NavLink></li>
                    <li><NavLink to='/login'>Войти</NavLink></li>
                </ul>
                {this.props.children}
            </div>
        )
    }
}

// export default class App extends Component {
//     render() {
//         return (
//             <div className='container'>
//                 <ul className='nav nav-pills'>
//                     <li><Link to='/admin' activeClassName='active'>Админка</Link></li>
//                     <li><Link to='/list' activeClassName='active'>Список жанров</Link></li>
//                 </ul>
//                 {this.props.children}
//             </div>
//         )
//     }
// }

// export default class App extends Component {
//     render() {
//         return <div className='container'>Привет из App!</div>
//     }
// }

// export default class App extends Component {
//     constructor(props) {
//         super(props)
//         this.state = {
//             route: window.location.hash.substr(1)
//         }
//     }
//     componentDidMount() {
//         window.addEventListener('hashchange', () => {
//             this.setState({
//                 route: window.location.hash.substr(1)
//             })
//         })
//     }
//     render() {
//         let Child
//
//         switch (this.state.route) {
//             case '/admin': Child = Admin; break;
//             case '/genre': Child = Genre; break;
//             default: Child = Home;
//         }
//
//         return (
//             <div className='container'>
//                 <h1>App</h1>
//                 <ul>
//                     <li><a href='#/admin'>Admin</a></li>
//                     <li><a href='#/genre'>Genre</a></li>
//                 </ul>
//                 <Child />
//             </div>
//         )
//
//     }
// }